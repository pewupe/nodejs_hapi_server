'use strict';

const Hapi = require('@hapi/hapi');
const routes = require('./routes');

const init = async () => {
    const server = Hapi.server({
        port: process.env.PORT,
        host: process.env.HOST
    });

    // routes
    server.route(
        [
            {
                method: 'GET',
                path: '/{name?}',
                handler: (request, h) => {
                    const name = request.params.name || 'world'

                    return `Hello, ${name}`;
                }
            }
        ].concat(
            routes
        )
    );

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (error) => {
    console.log('unhandledRejection error');
    console.log(error);
    process.exit(1);
});

init();