const json_response = 'application/json';
const sensors = [
    { id: 'S1', name: 'Temp in kitchen', type: 'Temperature', measure_unit: 'Celsius degrees' },
    { id: 'S2', name: 'Outside humidity', type: 'Humidity', measure_unit: 'AH' },
    { id: 'S3', name: 'Gas or smoke in garage', type: 'Gas and smoke', measure_unit: 'PPM' }
];

const sensorRouting = [
    {
        method: 'GET',
        path: '/sensors/{id?}',
        handler: (request, h) => {
            const { id } = request.params;
            let response;

            if (id) {
                const sensor = sensors.filter(sensor => sensor.id === id);

                if (sensor.length) {
                    response = h.response(JSON.stringify(sensor));
                    response.type(json_response);
                    return response;
                } else {
                    response = h.response(JSON.stringify({
                        error: 'Not Found',
                        message: `Sensor with id: ${id}, not found`
                    }));
                    response.type(json_response);
                    response.code(404);
                    return response;
                }
            } else {
                response = h.response(JSON.stringify(sensors));
                response.type(json_response);
                return response;
            }
        }
    },
    {
        method: 'PUT',
        path: '/sensors',
        handler: (request, h) => {
            let response;

            try {
                const sensor_object = request.payload;

                sensor_object.id = `S${sensors.length + 1}`;
                sensors.push(sensor_object);

                response = h.response(JSON.stringify(sensors[sensors.length - 1]));
                response.type(json_response);
                return response;
            } catch (error) {
                response = h.response(error.toString());
                response.code(400);
                return response;
            }
        }
    }
];

module.exports = sensorRouting;